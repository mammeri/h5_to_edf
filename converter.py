#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import files_from_3d
import argparse
import sys
import os



def makeArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument('mount')
    parser.add_argument('h5file')
    parser.add_argument('scan')
    parser.add_argument('--scantime',dest="scantime", default='None')
    parser.add_argument('--scancurrent',dest="scancurrent", default='None')
    parser.add_argument('--timecurrent',dest="timecurrent", default='None')
    parser.add_argument("--stem", dest="stem", default='data')
    parser.add_argument("--format", dest='format', default='edf')
    parser.add_argument("--startangle", dest='startangle', default = 0.0)
    parser.add_argument("--stepangle", dest='stepangle', default = 0.25)
    parser.add_argument("--expotime", dest='expotime', default=1.0)
    parser.add_argument("--wavelength", dest='wavelength', default=0.308)
    parser.add_argument("--x0", dest='x0', default=1024.)
    parser.add_argument("--y0", dest='y0', default=1024.)
    parser.add_argument("--distance", dest='distance', default=140.)
    parser.add_argument("--run", dest='run', default = 1)
    parser.add_argument("--dump", dest='dump', default=False, action='store_true')
    parser.add_argument("--zseries", dest='zseries', default=None)
    return parser


if __name__ == '__main__':
    parser = makeArgs()
    args = parser.parse_args()
    main(args)

