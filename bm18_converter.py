#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 11:30:54 2021
Modified on Wed Jul 28 15:24:00 2021

@author: boller, mammeri
"""
import os
import h5py
#import parser
import sys
from glob import glob

args = sys.argv
zmot_name = "sz"
# /data/bm18/inhouse/blctest/bm18/HA_600_42um_LADAF_2021_17_brain_122keV_0
#################################################################################
if len(args) >= 3:
    if "*" in args[1]:
        h5_names = [glob(i + "/*.h5")[0] for i in glob(args[1])]
        h5_names.sort()
    else:
        h5_names = [args[1]]
    edf_directory = args[2]
    scan_desc_number = 1
    if len(args) == 4:
        scan_desc_number = int(args[3])
        pass
else:
    raise ArgumentError(
        'Command line should be: python bm18_converter file.h5 output_directory\n Or : python bm18_converter "directory_00*" output_directory'
    )

for h5_name in h5_names:
    print(h5_name)
    #################################################################################
    # directory = '/data/bm18/inhouse/blctest/bm18/HA_600_42um_LADAF_2021_17_brain_122keV_001/HA_600_42um_LADAF_2021_17_brain_122keV_001_0001'
    h5_name = os.path.abspath(h5_name)
    directory = os.path.dirname(h5_name)
    # dataset   = 'HA_600_42um_LADAF_2021_17_brain_122keV_001_0001'
    dataset = directory.split("/")[-1]
    desc = str(scan_desc_number) + ".1"
    dark = str(scan_desc_number + 1) + ".1"
    flat = str(scan_desc_number + 2) + ".1"
    fast_acq = str(scan_desc_number + 3) + ".1"
    slow_acq = str(scan_desc_number + 3) + ".2"

    #program = parser.__file__
    dataset_output = os.path.join(edf_directory, dataset + "_")
    rights = os.popen(f"ls -l {edf_directory}").read()
    rights = rights.split("\n")
    processed = False
    for right in rights:
        if dataset in right and "rwxrwxrwx" in right:
            processed = True
    if processed:
        continue

    try:
        f = h5py.File(h5_name, "r")
        info = [i for i in f][0]
        end_time = info + "/end_time"
        if end_time not in f:
            f.close()
            raise Exception("Scan not over")
        f.close()
    except Exception as e:
        print(e)
        continue

    with h5py.File(h5_name, "r") as f:
        values = [i for i in f]
        dic_h5 = {}
        for value in values:
            dic_h5[value[-3:]] = value

        cameratype = f[dic_h5[desc] + "/technique/detector/edgehslin1/name"][()]
        try:
            cameratype = cameratype.decode()
        except:
            pass
        ref_on = f[dic_h5[desc] + "/technique/scan/tomo_n"][()]
        n_flat = f[dic_h5[desc] + "/technique/scan/flat_n"][()]
        energy = f[dic_h5[desc] + "/technique/scan/energy"][()]
        distance = f[dic_h5[desc] + "/technique/scan/sample_detector_distance"][()]
        scan_range = f[dic_h5[desc] + "/technique/scan/scan_range"][()]
        dark_n = f[dic_h5[desc] + "/technique/scan/dark_n"][()]
        y_step = f[dic_h5[flat] + "/technique/flat/displacement"][()]
        dim = f[dic_h5[desc] + "/technique/detector/edgehslin1/size"][()]
        tomo_exptime = f[dic_h5[desc] + "/technique/scan/exposure_time"][()]
        latency_time = f[dic_h5[desc] + "/technique/scan/latency_time"][()]
        roi = f[dic_h5[desc] + "/technique/detector/edgehslin1/roi"][()]
        try:
            acq_mode = f[dic_h5[fast_acq] + "/instrument/edgehslin1/acq_parameters/acq_mode"][()]
            max_expo = f[dic_h5[fast_acq] + "/instrument/edgehslin1/ctrl_parameters/acc_max_expo_time"][()]
            count_time = f[dic_h5[fast_acq] + "/instrument/edgehslin1/acq_parameters/acq_expo_time"][()]
            acq_frame = count_time / max_expo
        except:
            acq_mode = "" 
            acq_frame = ""
            max_expo = ""
        col_end = roi[0]
        col_beg = roi[1]
        row_end = roi[2]
        row_beg = roi[3]
        pixelsize = f[dic_h5[desc] + "/technique/optic/sample_pixel_size"][()]
        date = str(f[dic_h5[desc] + "/start_time"][()])
        srcurrent = f[dic_h5[desc] + "/instrument/machine/current"][()]
        try:
            comment = str(f[dic_h5[desc] + "/technique/scan/comment"][()])
        except:
            comment = ""
        scantype = str(f[dic_h5[desc] + "/technique/scan/field_of_view"][()])
        yrot = f[dic_h5[desc] + "/instrument/positioners/yrot"][()]

        print(directory)
        if not os.path.isdir(edf_directory):
            cmd = "mkdir " + edf_directory
            os.system(cmd)

        if not os.path.isdir(dataset_output):
            cmd = "mkdir " + dataset_output
            os.system(cmd)

        # Creation of projections
        cmd = f"python3 {program} {dataset_output} {h5_name} " + \
        f"{dic_h5[fast_acq]}/measurement/{str(cameratype)} " + \
        f"--scantime {dic_h5[fast_acq]}/measurement/timer_trig " + \
        f"--scancurrent {dic_h5[slow_acq]}/measurement/current " + \
        f"--timecurrent {dic_h5[slow_acq]}/measurement/elapsed_time " +\
        f"--format=edf --stem={dataset} --dump " + \
        f"--sx {dic_h5['4.1']}/instrument/positioners/sx " + \
        f"--sy {dic_h5['4.1']}/instrument/positioners/sy " + \
        f"--sz {dic_h5['4.1']}/instrument/positioners/sz"
        print(cmd)
        print("Conversion of projections in progress...")
        os.system(cmd)
        print("Conversion of projections done")

        # Creation of darks
        cmd = (
            "python3 "
            + program
            + " "
            + dataset_output
            + " "
            + h5_name
            + " "
            + dic_h5[dark]
            + "/measurement/"
            + cameratype
            + " --format=edf --stem=dark --dump"
        )
        print("Conversion of darks in progress...")
        os.system(cmd)
        cmd = "mv " + dataset_output + "/dark0000.edf " + dataset_output + "/dark.edf"
        os.system(cmd)
        print("Conversion of darks done")

        # Creation of references
        cmd = (
            "python3 "
            + program
            + " "
            + dataset_output
            + " "
            + h5_name
            + " "
            + dic_h5[flat]
            + "/measurement/"
            + cameratype
            + " --format=edf --stem=ref --dump"
        )
        print(cmd)
        print("Conversion of references in progress...")
        os.system(cmd)
        cmd = (
            "mv "
            + dataset_output
            + "/ref0000.edf "
            + dataset_output
            + "/refHST0000.edf"
        )
        os.system(cmd)

        print("Conversion of references done")

        # Creation of scan.info
        print("Creation of the .info file")
        infofile = dataset_output + "/" + dataset + "_.info"
        if os.path.isfile(infofile):
            f = open(infofile, "r")
            lines = [
                line.strip("\n") for line in f.readlines()
            ]  # on retire les \n en fin de ligne avec strip('\n')
        else:
            lines = [""] * 40

        lines[1] = "Energy= " + str(energy)
        lines[2] = "Distance= " + str(distance)
        lines[3] = "Prefix= " + dataset
        lines[4] = "Directory= " + dataset_output
        lines[5] = "ScanRange= " + str(scan_range)
        lines[6] = "TOMO_N= " + str(ref_on)
        lines[7] = "REF_ON= " + str(ref_on)
        lines[8] = "REF_N= " + str(n_flat)
        lines[9] = "DARK_N= " + str(dark_n)
        lines[10] = "Y_STEP= " + str(y_step)
        lines[11] = "Dim_1= " + str(dim[0])
        lines[12] = "Dim_2= " + str(dim[1])
        lines[13] = "Count_time= " + str(tomo_exptime / 1000)
        lines[14] = "Latency_time (s)= " + str(latency_time / 1000)
        lines[16] = "Col_end= " + str(col_end)
        lines[17] = "Col_beg= " + str(col_beg)
        lines[18] = "Row_end= " + str(row_end)
        lines[19] = "Row_beg= " + str(row_beg)
        lines[21] = "PixelSize= " + str(pixelsize)
        lines[22] = "Optic_used= " + str(pixelsize)
        lines[23] = "Date= " + str(date[2:-1])
        lines[26] = "SrCurrent= " + str(f"{srcurrent:.3f}")
        lines[29] = "Acq_mode= " + str(acq_mode, "utf-8")
        lines[30] = "Acq_nb_frame= " + str(acq_frame)
        lines[31] = "Max_expo_time= " + str(max_expo)
        lines[38] = "Comment= " + str(comment[2:-1])

        if os.path.isfile(infofile):
            f.close()

            # infofile='EDF/'+dataset+'/'+dataset+'good.info'
        with open(infofile, "w") as filout:
            for line in lines:
                filout.write(line + "\n")

        try:
            last_proj = ref_on - 1
            cmd = f"cp {dataset_output}/{dataset}_{last_proj}.edf {dataset_output}/{dataset}_{ref_on}.edf"
            os.system(cmd)

            cmd = f"cp {dataset_output}/refHST0000.edf {dataset_output}/refHST{ref_on}.edf"
            os.system(cmd)

            cmd = f"chmod -R 777 {dataset_output}"
            os.system(cmd)
        except Exception as e:
            print(f"You might need to create {dataset_output}/{dataset}_{ref_on}.edf and {dataset_output}/refHST{ref_on}.edf")
            print(e)
            
        print("=============================================")
        print("Congrats! H5 scan has been converted in EDF")
        print("=============================================")
        print("Important scan parameters:")
        print("=============================================")
        print("Scan type: " + scantype[2:-1])
        print("Pixel size: " + str(pixelsize))
        print("yrot: " + str(yrot))
        ha = yrot * 1000 / pixelsize
        print("HA: " + str(round(ha)))
        print("Projection size: " + str(dim[0]) + "x" + str(dim[1]))
        slicesize = round(dim[0] / 2 + ha) * 2
        print("You should obtain slices: " + str(slicesize) + "x" + str(slicesize))
        print("Scan in:" + "EDF")
    

